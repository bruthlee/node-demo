const pathList = [
    '/a/b',
    '/a/2/c',
    '/d'
]
/*
const root = {
    a: {
        2: {
            c: {}
        },
        b: {}
    },
    d: {}
}
*/

let root = {}
pathList.map(item => {
    let temp = root
    item.split('/').slice(1).forEach(key => {
        temp[key] || (temp[key] = {})
        temp = temp[key]
    })
})
console.log(root)