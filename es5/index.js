function Parent() {
  this.name = ['kevin'];
}
Parent.prototype.getName = function () {
  console.log(this.name, this.age);
}
function Child(age) {
  Parent.call(this);
  this.age = age;
}
Child.prototype = new Parent();
var child1 = new Child(19);
var child2 = new Child(20);
child1.name.push("cc");
child1.getName();  //["kevin", "cc"] 19    
child2.getName();  //["kevin"] 20