var counter = 3
function incounter() {
    return ++counter
}
function getCounter() {
    return counter
}
module.exports = {
    // counter
    get counter() {
        return counter
    },
    incounter
}