exports.types = [
    'String',
    'Number',
    'Function',
    'Object',
    'Array',
    'Boolean',
    'Map',
    'Set',
    'Date',
    'Symbol',
    'RegExp',
    'Error'
]