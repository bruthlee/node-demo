// 支持循环引用的简单深拷贝
function getType(obj) {
    const t = Object.prototype.toString.call(obj)    
    
}

function isObject(obj) {
    
}

function clone(obj, map = new WeakMap()) {
    if (typeof obj === 'object') {
        if (map.get(obj)) {
            return map.get(obj)
        }

        const temp = Array.isArray(obj) ? [] : {}
        map.set(obj, temp)

        for (const key in obj) {
            temp[key] = clone(obj[key], map)
        }
        return temp
    }
    return obj
}

const target = {
    a: 1,
    b: 'bb',
    c: {
        k: 'cc',
        v: 99
    },
    d: [1,2,3],
    e: [
        {
            e: 'ee'
        },{
            e: 'eee'
        }
    ]
}

const b = clone(target)
b.b = '3b'
b.t = target
console.log('target: ', target)
console.log('=============================')
console.log('b: ', b)