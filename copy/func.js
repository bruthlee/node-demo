// 函数传参值拷贝
function test(obj) {
    console.log('1 obj: ', obj)
    obj.test = 'test'
    console.log('2 obj: ', obj)
    obj = {
        a: 'a',
        b: 'b'
    }
    console.log('3 obj: ', obj)
}

const obj = {
    k: 'kk',
    v: 99
}
console.log('4 obj: ', obj)
test(obj)
console.log('5 obj: ', obj)
