// 支持基础类型、数组、对象的简单深拷贝
function clone(obj) {
    if (typeof obj === 'object') {
        const temp = Array.isArray(obj) ? [] : {}
        for (const key in obj) {
            temp[key] = clone(obj[key])
        }
        return temp
    }
    return obj
}

const target = {
    a: 1,
    b: 'bb',
    c: {
        k: 'cc',
        v: 99
    },
    d: [1,2,3],
    e: [
        {
            e: 'ee'
        },{
            e: 'eee'
        }
    ]
}

const b = clone(target)
b.b = '3b'
b.c.v = 199
b.d[2] = 99
b.e[0].e = '5ee'
console.log('target: ', target)
console.log('=============================')
console.log('b: ', b)