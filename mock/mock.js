// 使用 Mock
var Mock = require('mockjs')
// var $ = require('jquery')
// $.mockJSON.data.US_STATE = [
//     'Alabama', 'Alaska', 'Wisconsin', 'Wyoming'
// ];
var data = Mock.mock({
    // 属性 list 的值是一个数组，其中含有 1 到 10 个元素
    'list|1-10': [{
        // 属性 id 是一个自增数，起始值为 1，每次增 1
        'id|+1': 1
    }]
})
// 输出结果
console.log(JSON.stringify(data, null, 4))


// data = Mock.mock({
//     'number1|1-100.1-10': 1,
//     'number2|123.1-10': 1,
//     'number3|123.3': 1,
//     'number4|123.10': 1.123
// })
// console.log(JSON.stringify(data, null, 4))


// data = Mock.mock({
//     'sex|1': true,
//     'per|90-100': 95
// })
// console.log(JSON.stringify(data, null, 4))

var Random = Mock.Random
Random.extend({
    constellation: function(date) {
        var constellations = ['白羊座', '金牛座', '双子座', '巨蟹座', '狮子座', '处女座', '天秤座', '天蝎座', '射手座', '摩羯座', '水瓶座', '双鱼座']
        return this.pick(constellations)
    }
})
data = Random.constellation()
console.log(data, ' --- ', Mock.mock('@CONSTELLATION'))

console.log('boolean: ', Random.boolean(1, 2, true))

console.log('natural: ', Random.natural(1, 10))

console.log('integer: ', Random.integer(1))

console.log('range: ', Random.range(1, 20, 2))

console.log('date: ', Random.date(), ', ', Random.date('yyyy/MM/dd HH:mm:ss'))

console.log('time: ', Random.time(), ', ', Random.time('A HH:mm:ss'))

console.log('datetime: ', Random.datetime())

console.log('image: ', Random.image())

console.log('color: ', Random.color())