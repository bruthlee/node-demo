module.exports = function({params, query, body}) {
    console.log('params: ', params)
    console.log('query: ', query)
    console.log('body: ', body)
    const data = {
        msg: 'This is one get message.',
        'users|5-10': [
            {
                'id': '@string("number", 2)',
                'name': '@csentence(2,3)',
                'icon|1': [
                    'https://img.26abc.com/d/file/touxiang/109_18394.jpg',
                    'https://img.26abc.com/d/file/touxiang/109_18397.jpg',
                    'https://img.26abc.com/d/file/touxiang/109_18393.jpg',
                    'https://img.26abc.com/d/file/touxiang/0ccl3m1hksp.jpg',
                    'https://img.26abc.com/d/file/touxiang/109_26988.jpg',
                    'https://img.26abc.com/d/file/touxiang/109_26994.jpg'
                ],
                'areaId': '@string("number", 3)',
                'areaName': '@province',
                'county': '@county(true)',
                'startDate': '@datetime("yyyy-MM-dd")',
                'endDate': '@datetime("yyyy-MM-dd")',
                'age': '@integer(20, 80)',
                'grade': /[1-9]\d{1}/
            }
        ]
    }
    if (query) {
        data.query = query
    }
    if (params) {
        data.params = params
    }
    return {
        code: 0,
        data: data,
        msg: 'success'
    }
}