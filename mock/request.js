var axios = require('axios')

const base = 'http://localhost:3000'

axios.interceptors.request.use(config => {
    console.log('config: ', config)
    return config
}, err => {
    console.log('interceptors request error: ', err)
    return Promise.reject(err)
})

axios.interceptors.response.use(res => {
    console.log('res: ', JSON.stringify(res))
    return res
}, err => {
    console.log('interceptors response error: ', err)
    return Promise.reject(err)
})