const LRU = require("lru-cache")
const options = { 
  max: 500, 
  length: function (n, key) { 
    return n * 2 + key.length 
  }, 
  dispose: function (key, n) { 
    // n.close() 
  }, 
  maxAge: 1000 * 60 * 60 
}
const cache = new LRU(options)
cache.set("key", "value")
const value = cache.get("key")
console.log(`v: ${value}`)

const someObject = { a: 1 }
cache.set(someObject, 'a value')
// Object keys are not toString()-ed
cache.set('[object Object]', 'a different value')
console.log('cache.get(someObject): ', cache.get(someObject))
console.log('cache.get({ a: 1 }): ', cache.get({ a: 1 }))

cache.reset()    // empty the cache