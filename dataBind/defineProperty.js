const obj = {
    name: 'test',
    age: 18,
    grade: 'A'
}

function defineReactive(obj, key, value) {
    Object.defineProperty(obj, key, {
        get: function() {
            // 写处理逻辑
            console.log('获取值: ', value)
            return value
        },
        set: function(newVal) {
            // 写处理逻辑
            console.log(key + '赋值：', newVal)
            if (newVal === value) return
            value = newVal
        }
    })
}

function observeObject(obj) {
    const keys = Object.keys(obj)
    for(let i = 0; i < keys.length; i++) {
        const key = keys[i]
        const value = obj[key]
        defineReactive(obj, key, value)
    }
}

observeObject(obj)
console.log('打印1：', obj.name)
obj.name = '天天向上'
console.log('打印2：', obj.name)
obj.age = 25
obj.grade = 'A+'

obj.other = 'other'
console.log('obj.other: ', obj.other)
/**
 * 获取值:  test
 * 打印1： test
 * name赋值： 天天向上
 * 获取值:  天天向上
 * 打印2： 天天向上
 * age赋值： 25
 * grade赋值： A+
 * obj.other:  other
 */