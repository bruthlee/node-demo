const obj = {
    name: 'test',
    age: 18,
    grade: 'A'
}

const proxy = new Proxy(obj, {
    get: function(target, key, receiver) {
        console.log(`获取${key}值: ${obj[key]}`)
        // 写处理逻辑
        return Reflect.get(target, key, receiver)
    },
    set: function(target, key, value, receiver) {
        console.log(`${key}赋值: ${value}`)
        // 写处理逻辑
        return Reflect.set(target, key, value, receiver)
    }
})

console.log('name: ', proxy.name)
proxy.age = 20
console.log('grade: ', proxy.grade)
/**
 * 获取name值: test
 * name:  test
 * age赋值: 20
 * 获取grade值: A
 * grade:  A
 */