function debounce(func, delay) {
    var timeout;
    return function(e) {
        clearTimeout(timeout);
        var context = this, args = arguments;
        timeout = setTimeout(() => {
            func.apply(context, args);
        }, delay);
    }
}

var validate = debounce(function(e) {
    console.log('change: ', e.target.value, new Date-0)
}, 300);

document.getElementById('account').addEventListener('input', validate);