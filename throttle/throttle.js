function throttle(func, throttleTime) {
    var timeout;
    var start = Date.now();
    var throttleTime = throttleTime || 160;
    return function() {
        clearTimeout(timeout);
        var context = this, args = arguments;
        if (Date.now() - start >= throttleTime) {
            func.apply(context, args);
            start = Date.now();
        }
        else {
            timeout = setTimeout(() => {
                func.apply(context, args);
            }, throttleTime);
        }
    }
}

var mousemove = throttle(function(e) {
    console.log(e.pageX, e.pageY);
}, 1000);

document.getElementById('panel').addEventListener('mousemove', mousemove);