const semver = require('semver')

let res = semver.valid('1.2.3')
console.log(res) // '1.2.3'

res = semver.valid('a.b.c')
console.log(res) // null

res = semver.clean('  =v1.2.3   ')
console.log(res) // '1.2.3'

res = semver.satisfies('1.2.3', '1.x || >=2.5.0 || 5.0.0 - 7.2.3')
console.log(res) // true

res = semver.gt('1.2.3', '9.8.7')
console.log(res) // false

res = semver.lt('1.2.3', '9.8.7')
console.log(res) // true

res = semver.minVersion('>=1.0.0')
console.log(res) // '1.0.0'
/**
 SemVer {
        options: {},
        loose: false,
        includePrerelease: false,
        raw: '1.0.0',
        major: 1,
        minor: 0,
        patch: 0,
        prerelease: [],
        build: [],
        version: '1.0.0' 
    }
 */

res = semver.valid(semver.coerce('v2'))
console.log(res) // '2.0.0'

res = semver.valid(semver.coerce('42.6.7.9.3-alpha'))
console.log(res) // '42.6.7'