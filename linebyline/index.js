const readline = require('linebyline')
const path = require('path')
const root = path.resolve(process.cwd(), 'linebyline')
let url = path.join(root, 'test.txt')
const rl = readline(url)
rl.on('line', function (line, lineCount, byteCount) {
    console.log(line)
})
.on('error', function (e) {
    // something went wrong
    console.log('err: ', e)
});

setTimeout(() => {
    url = path.join(root, 'test.vue')
    const rl1 = readline(url)
    rl1.on('line', function (line, lineCount, byteCount) {
        // do something with the line of text
        console.log('===================================')
        console.log(line)
        console.log(lineCount)
        console.log(byteCount)
    })
    .on('error', function (e) {
        // something went wrong
        console.log('err: ', e)
    })
}, 1000)