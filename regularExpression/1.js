function computeCellData(func) {
    // 'SUM(a1,a2:a5)-a10'
    const res = func.match(/(\()(.*)(\))/);
    // ['(a1,a2:a5)', '(', 'a1,a2:a5', ')']
    if (res && res.length > 2) {
        const arr = [];
        res[2].split(',').map(one => {
            if (one.indexOf(':') > 0) {
                const oneArr = one.split(':');
                const start = parseInt(oneArr[0].replace(/[^0-9]/g, ""));
                const end = parseInt(oneArr[1].replace(/[^0-9]/g, ""));
                const key = oneArr[0].replace(/[0-9]/g, "");
                for (let index = start; index <= end; index++) {
                    arr.push(`${key}${index}`);
                }
            }
            else {
                arr.push(one);
            }
        });
        console.log('arr: ', arr);
        func = func.replace(res[2], arr.join('+')).replace('SUM','').replace('(','').replace(')','');
        console.log('func: ', func);// 'SUM(a1,a2,a3,a4,a5)-a10' 
    }
    return '';
}

const target = 'SUM(a1,a2:a5)-a10';
computeCellData(target);