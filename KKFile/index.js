// debugger
document.body.oncontextmenu = function() {
    return false;
}
document.body.onselectstart = function() {
    return false;
}
document.body.onkeydown = function() {
    return false;
}
document.oncontextmenu = function () {
    return false;
}

function dateFormat(fmt, date) {
    var ret;
    var opt = {
        'Y+': date.getFullYear().toString(), // 年
        'm+': (date.getMonth() + 1).toString(), // 月
        'd+': date.getDate().toString(), // 日
        'H+': date.getHours().toString(), // 时
        'M+': date.getMinutes().toString(), // 分
        'S+': date.getSeconds().toString(), // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (const k in opt) {
        ret = new RegExp('(' + k + ')').exec(fmt);
        if (ret) {
            fmt = fmt.replace(
                ret[1],
                ret[1].length === 1 ? opt[k] : opt[k].padStart(ret[1].length, '0')
            );
        }
    }
    return fmt;
}

function openUrl(path) {
    // http://47.108.251.213:10025/onlinePreview?url=aHR0cHM6Ly9vZmZpY2V3ZWIzNjUuY29tL3ZpZXdmaWxlL%2BWFs%2BS6juWKoOW%2Fq%2BS4tOaXtuiuvuaWveW7uuiuvumAn%2BW6puWSjOS%2FneivgeW7uuiuvuagh%2BWHhueahOmAmuefpS5kb2N4
    var url = 'http://47.108.251.213:10026/api/onlinePreview?url=';
    var date = dateFormat('YYYY-mm-dd HH:MM:SS', new Date());
    var water = date + ' 张三';
    url += encodeURIComponent(Base64.encode(path)) + '&watermarkTxt=' + encodeURIComponent(water);
    // console.log(url, ' <-> ', water);
    var width = screen.availWidth - 10;
	var height = screen.availHeight - 100;
	var szFeatures = "top=0,";
	szFeatures +="left=0,";
	szFeatures +="width="+width+",";
	szFeatures +="height="+height+",";
	szFeatures +="directories=no,";
	szFeatures +="status=yes,toolbar=no,location=no,";
	szFeatures +="menubar=no,";
	szFeatures +="scrollbars=yes,";
	szFeatures +="resizable=yes";
	window.open(url,"",szFeatures);
}