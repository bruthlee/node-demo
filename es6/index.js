function toArr(list, start) {
    start = start || 0;
    let i = list.length - start;
    const ret = new Array(i);
    while (i--) {
        ret[i] = list[i + start];
    }
    return ret;
}

function use(plugin) {
    const plugins = (this._installed_plugins || (this._installed_plugins = []));
    if (plugins.indexOf(plugin) > -1) {
        return this;
    }
    const arg = toArr(arguments, 1);
    arg.unshift(this);
    if (typeof plugin.install === 'function') {
        plugin.install.apply(plugin, arg);
    }
    else if (typeof plugin == 'function') {
        plugin.apply(null, arg);
    }
    plugins.push(plugin);
    return this;
}