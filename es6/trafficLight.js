function red() {
    console.log('red');
}

function green() {
    console.log('green');
}

function yellow() {
    console.log('yellow');
}

const task = (func, time) => {
    return new Promise(resolve => {
        setTimeout(() => {
            func()
            resolve()
        }, time);
    })
}

const step = () => {
    Promise.resolve().then(() => {
        return task(red, 3000)
    }).then(() => {
        return task(green, 5000)
    }).then(() => {
        return task(yellow, 2000)
    }).then(() => {
        step()
    })
}
step()
