const program = require('commander')
program.version('0.0.1', '-v, --vers', 'output the current version')
console.log('version: ', program.version())
/**
 * console.log('program: ', program)
 * 
Command {
  commands: [],
  options:
   [ Option {
       flags: '-V, --version',
       required: false,
       optional: false,
       bool: true,
       short: '-V',
       long: '--version',
       description: 'output the version number' } ],
  _execs: {},
  _allowUnknownOption: false,
  _args: [],
  _name: '',
  Command:
   { [Function: Command]
     super_:
      { [Function: EventEmitter]
        EventEmitter: [Circular],
        usingDomains: false,
        defaultMaxListeners: [Getter/Setter],
        init: [Function],
        listenerCount: [Function] } },
  Option: [Function: Option],
  _version: '0.0.1',
  _versionOptionName: 'version',
  _events: [Object: null prototype] { 'option:version': [Function] },
  _eventsCount: 1 }
*/