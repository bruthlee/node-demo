const fetch = require('node-fetch')

async function load(urls) {
  // 并发获取网页内容
  const contents = urls.map(async function(url){
    const resp = await fetch(url)
    return resp.text()
  })
  
  // 按顺序输出
  for (let resp of contents) {
    const content = await resp
    const title = content.match(/<title>([\s\S]+)<\/title>/i)[1]
    console.log(title)
  }
}

const urls = [
  'http://www.baidu.com',
  'http://www.taobao.com',
  'http://www.tmall.com/'
]
load(urls)