const fs = require('fs');

const timeoutScheduled = Date.now();

// 异步任务一：100ms 后执行的定时器
setTimeout(() => {
  const delay = Date.now() - timeoutScheduled;
  console.log(`time: ${delay}ms`);
}, 100);

// 异步任务二：文件读取后，有一个 200ms 的回调函数
fs.readFile('demo.js', () => {
    setTimeout(() => console.log('aaa'));
    setImmediate(() => console.log('bbb'));
//   const startCallback = Date.now();
//   const t = Date.now() - startCallback;
//   while (t < 200) {
//     // console.log('i/o: ' + t)
//   }
});


setTimeout(() => console.log('1'));
setImmediate(() => console.log('2'));
console.log('3');
new Promise((re, rj) => {
    console.log('4')
    re()
}).then(()=> {
    console.log('5')
})
//浏览器： 3 -> 4 -> 5 -> 2 -> 1
//node： 3 -> 4 -> 5 -> 1 -> 2 -> bbb -> aaa -> 102ms