const http = require('http')
const url = require('url')
const util = require('util')

exports.start = function(router, handle) {
    http.createServer(function(request, response) {
        // if (request.url === '/upload' && request.method.toLowerCase() === 'post') {
        //     var form = new formidable.IncomingForm()
        //     form.parse(request, function(err, fileds, files) {
        //         response.writeHead(200, {'content-type': 'text/plain'})
        //         response.write('received upload: \n\n')
        //         response.end(util.inspect({fileds: fileds, files: files}))
        //     })
        //     return
        // }

        // response.writeHead(200, {'content-type': 'text/html'}); 
        // response.end(
        //     '<form action="/upload" enctype="multipart/form-data" '+ 'method="post">'+
        //     '<input type="text" name="title"><br>'+
        //     '<input type="file" name="upload" multiple="multiple"><br>'+ '<input type="submit" value="Upload">'+
        //     '</form>'
        // );

        const path_name = url.parse(request.url).pathname
        console.log("Request for " + path_name + " received.")
        router(handle, path_name, response, request)

        // var postData = "";
        // request.setEncoding("utf8");
        // request.addListener("data", function(postDataChunk) {
        //     postData += postDataChunk;
        //     console.log("Received POST data chunk '" + postDataChunk + "'.");
        // });
        // request.addListener("end", function() {
        //     router(handle, path_name, response, postData);
        // });

    }).listen(8080)
    console.log('Server has started.')
}