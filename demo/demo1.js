console.log('【1】考察模版函数输出问题：第一个参数一定是模板里面的字符串数组，后面的参数依次是输入参数')
function getPersonInfo(one, two, three) {
    console.log('one: ', one) // [ '', ' is ', ' years old.' ]
    console.log('two: ', two) // Lydia
    console.log('three: ', three) // 12
}
const person = 'Lydia'
const age = 12
getPersonInfo`${person} is ${age} years old.`
console.log('========= END =============')


console.log('【2】考察let var const 变量声明：代码依次执行，var全局变量被提前&读不到值，let和const不能提前引用')
function sayHi() {
    console.log('var name is ',name) // undefined
    console.log('let age is ', age) // ReferenceError: age is not defined
    console.log('const sex is ', sex) // ReferenceError: sex is not defined
    var name = 'Lydia'
    let age = 12
    const sex = '女'
}
// sayHi()
console.log('========= END =============')

console.log('【3】考察set函数：数组去重')
const set = new Set([1,1,1,2,3,4])
console.log('set is ', set) // Set { 1, 2, 3, 4 }
console.log('========= END =============')


console.log('【4】考察JS假值问题:0、-0、null、NAN、空字符串、undefined')
const zero = 0  // false
const zero_num = new Number(0) // true
const space_no = '' // false
const space_one = ' ' // true
const bool_false = new Boolean(false) // true
const undefine_value = undefined // false
console.log(Boolean(zero), Boolean(zero_num), Boolean(space_no), Boolean(space_one), Boolean(bool_false), Boolean(undefine_value))
console.log('========= END =============')



console.log('【5】考察static函数: static声明类函数，只能通过类调用，不能通过实例')
class Chameleon {
    static colorChange(newColor) {
        console.log(this.newColor, ' -1- ', newColor) // undefined ' -1- ' 'red'
        this.newColor = newColor
        console.log(this.newColor, ' -2- ', newColor) // red  -2-  red
    }
    static show() {
        console.log('Chameleon: ', Chameleon) // class Chameleon
    }
    constructor({newColor = 'green'} = {}) {
        console.log(this.newColor, ' -3- ', newColor) // undefined ' -3- ' 'purple'
        this.newColor = newColor
        console.log(this.newColor, ' -4- ', newColor) // purple  -4-  purple
    }
    show() {
        console.log('freddie: ', this) // Chameleon { newColor: 'purple' }
        console.log('newColor: ', this.newColor) // purple
    }
}
const freddie = new Chameleon({ newColor: 'purple'})
Chameleon.colorChange('red')
freddie.show()
Chameleon.show()
// freddie.colorChange('orange')// TypeError: freddie.colorChange is not a function
console.log('========= END =============')



console.log('【6】考察Promise')
console.log(Promise.resolve(5)) // Promise { 5 }
console.log('========= END =============')



console.log('【7】考察变量引用: var声明最终值，let声明当前引用值')
for (var i = 0; i < 3; i++) {
    setTimeout(() => {
        console.log(i)
    }, 1); 
}
for (let i = 0; i < 3; i++) {
    setTimeout(() => {
        console.log(i)
    }, 1); 
}
setTimeout(() => {
    console.log('========= END =============')
}, 3);


console.log('【8】考察方法参数引用')
console.log('I want pizza'[0])
console.log('========= END =============')