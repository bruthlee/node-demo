async function async1() {
    console.log('async1 start.')
    await async2()
    console.log('async1 end.')
}

async function async2() {
    console.log('async2.')
}

console.log('script start.........')

setTimeout(() => {
    console.log('setTimeout')
}, 0);

async1()

new Promise(function(resolve) {
    console.log('promise resolve.')
    resolve()
}).then(() => {
    console.log('promise then.')
})

console.log('script end...........')

/**
 * 
script start.........
async1 start.
async2.
promise resolve.
script end...........
async1 end.
promise then.
setTimeout
 */