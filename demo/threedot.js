const arr_num = [1, 2, 3, 4, 5]
const arr_num_copy = [...arr_num]
arr_num_copy[3] = 100
const arr_num_from = Array.from(arr_num)
arr_num_from[2] = 99
console.log(arr_num, ',', arr_num_copy, ',', arr_num_from)
console.log('=====================================================================')

const arr_obj = [
    {
        name: 'kevin',
        age: 19
    },
    {
        name: 'bruthlee',
        age: 20
    },
]
const arr_obj_copy = [...arr_obj]
arr_obj_copy[1].age = 99
const arr_obj_from = Array.from(arr_obj)
arr_obj_from[1].name = 'kitty'
console.log(arr_obj)
console.log(arr_obj_copy)
console.log(arr_obj_from)
console.log('=====================================================================')

const arr_fix = [1, 2, 'a', true, null, 'xy']
const arr_fix_copy = [...arr_fix]
arr_fix_copy[2] = 99
arr_fix_copy[3] = 'abc'
arr_fix_copy[4] = 8
console.log(arr_fix)
console.log(arr_fix_copy)