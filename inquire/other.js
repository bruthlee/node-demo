const inquirer = require('inquirer')
inquirer.prompt([{
        type: 'input',
        message: '请选择是否创建（y/n）',
        name: 'xory',
        default: 'y',
    },
    {
        type: 'list',
        message: '你喜欢什么水果？',
        name: 'xig',
        default: '0',
        choices: ['西瓜', '苹果', '香蕉']
    },
    {
        type: 'input',
        message: '请输入手机号:',
        name: 'phone',
        validate: function (val) {
            if (val.match(/\d{11}/g)) { // 校验位数
                return true;
            }
            return "请输入11位数字";
        }
    }
]).then(answer => {
    console.log(answer); // answer就是用户的输入
})