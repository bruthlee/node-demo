const inquirer = require('inquirer')
inquirer.prompt([
    /* Pass your questions in here */
    {
        name: 'name',
        type: 'input',
        message: `1.Whats\' your name?`
    },
])
.then((res) => {
    // Use user feedback for... whatever!!
    console.log('Name is : ', res.name)
    inquirer.prompt({
        name: 'sex',
        type: 'list',
        choices: [
            'Male',
            'Female'
        ]
    }).then(res => {
        console.log('Sex is : ', res.sex)
        inquirer.prompt({
            name: 'number',
            type: 'rawlist',
            choices: [
                '1',
                '5',
                '10'
            ]
        }).then(res => {
            console.log('Number is : ', res.number)
            inquirer.prompt({
                name: 'fav',
                type: 'checkbox',
                choices: [
                    'Music',
                    'Movie',
                    'Book'
                ]
            }).then(res => {
                console.log('Favorite is : ', res.fav)
                inquirer.prompt({
                    name: 'join',
                    type: 'confirm',
                    message: 'Do you want to join the meeting?'
                }).then(res => {
                    console.log('Join the meeting : ', res.join)
                })
            })
        })
    })
})